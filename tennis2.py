from pyomo import environ as pe
from pyomo import gdp as pg

import visualization

def lookup_available(players, player, day, time):
    return time in players[player]['available'].get(day, [])

class ModelInput():
    def __init__(self, player_data):
        super().__init__()
        self.player_data = player_data
        self.available = dict(((player, day, time), lookup_available(player_data, player, day, time))
                                 for player in player_data.keys()
                                 for day in weekdays
                                 for time in times)  
        self.num_sessions = dict((player, player_data[player]['sessions'])
                                 for player in player_data.keys())

    def player_names(self):
        return self.player_data.keys()

weekdays = ["mon", "tue", "wed", "thu", "fri"]
times = [15, 16, 17, 18]

def unassigned_players(model):
    unassigned = 0
    for p in model.players:
        assigned = False
        for d in weekdays:
            for t in times:
                assigned = assigned or model.assignments[p][d][t]
        if not assigned:
            unassigned += 1
    return unassigned


def time_rule(model, player, day, time):
    return time in model.available[player][day][time]

def respect_player_max_classes(model, player):
    n_classes = sum(model.assignments[player, day, time]
                    for day in model.days for time in model.times)
    return n_classes <= model.num_sessions[player]

def max_one_class_per_day(model, player, day):
    daily_classes = sum(model.assignments[player, day, time] 
                        for time in model.times) 
    return daily_classes <= 1

def max_players_per_session(model, day, time):
    sessions = sum(model.assignments[player, day, time]
                    for player in model.players)
    return sessions <= 5


def build_model(model_input):
    model = pe.ConcreteModel()
    
    model.players = pe.Set(initialize = model_input.player_names())
    model.days = pe.Set(initialize = weekdays)
    model.times = pe.Set(initialize = times)
    model.sessions = pe.Set(initialize = [1,2])

    model.assignments = pe.Var(model.players * model.days * model.times, 
                               within = pe.Binary)
    
    model.available = pe.Param(model.players * model.days * model.times,
                           initialize = model_input.available,
                           within=pe.Binary)
    model.num_sessions = pe.Param(model.players,
                                  initialize = model_input.num_sessions,
                                  within=model.sessions)


    model.repspect_number_classes = pe.Constraint(model.players, rule=respect_player_max_classes)

    model.max_one_class_per_day = pe.Constraint(model.players, model.days, rule=max_one_class_per_day)

    model.max_players_per_session = pe.Constraint(model.days, model.times, rule=max_players_per_session)

    model.obj = pe.Objective(
        expr = pe.summation(model.available, model.assignments),
        sense = pe.maximize)

    return model

def schedule_solve(model):
    pe.SolverFactory('cbc').solve(model)
    # results = [{{p,d,t}: model.assignments[p,d,t]()} 
    #             for p in model.players
    #             for d in model.days
    #             for t in model.times]
    results = [
        {'Player': p, 'Day': d, 'Time': t}
        for p in model.players for d in model.days for t in model.times if model.assignments[p,d,t]()>0
    ]
    return results


#res = schedule_solve(model)


#print(visualization.visualize(res))