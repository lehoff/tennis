import pandas as pd

def visualize(result):
    schedule = pd.DataFrame(result)
    print(schedule.sort_values(by=['Day', 'Time']).set_index(['Day', 'Time']))


"""
    DAYS = ['mon', 'tue', 'wed', 'thu', 'fri']
    PERSONS = list(schedule['Player'].unique())
    TRAINING_TIME = list(schedule['Time'].max())
"""