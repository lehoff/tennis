import random

def player_name(i):
    return f'p{i}'

def player_names(n):
    return [player_name(i) for i in range(n)]

def day_times():
    return random.sample(list(range(15,19)), random.randint(0,4))

DAYS=['mon', 'tue', 'wed', 'thu', 'fri']

def week_times():
    return {day: day_times() for day in DAYS}

def desired_sessions():
    return random.randint(1, 2)

def player_data():
    return {'available': week_times(), 'sessions': desired_sessions()}

def players(n):
    return {player_name(i): player_data() for i in range(n)}

