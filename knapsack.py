from pyomo.environ import *
items = ['hammer', 'wrench', 'screwdriver', 'towel']
v = {'hammer':8, 'wrench':3, 'screwdriver':6, 'towel':11}
w = {'hammer':5, 'wrench':7, 'screwdriver':4, 'towel':3}
W_max = 14

def x_domain(model, i):
    if i == 'towel':
        return IntegerInterval(bounds=(0,1))
    else:
        return IntegerInterval(bounds=(0,3))

model = ConcreteModel()
model.x = Var( items, within=x_domain )
model.value = Objective(
   expr = sum( v[i]*model.x[i] for i in items ),
   sense = maximize )
model.weight = Constraint(
   expr = sum( w[i]*model.x[i] for i in items ) <= W_max )