from pyomo import environ as pe

players = {
    "a": {"available": {"monday": [15, 16, 17], "tuesday": [15]}},
    "b": {"available": {"tuesday": [15, 17], "wednesday": [16, 17]}}
}

player_names = players.keys()

available = dict()

for player in player_names:
    available[player] = players[player]['available']

print(available)

weekdays = ["monday", "tuesday", "wednesday", "thursday", "friday"]
times = [15, 16, 17, 18]

timeslots = [(day, time) for day in weekdays for time in times]
timeslot_index = dict(enumerate(timeslots, start=1))


def time_rule(model, i):
    (day, time) = timeslot_index[index]
    if day in model.available[i]:
        return time in model.available[i][day]
    else:
        return False


def time_domain(model, i):
    return pe.IntegerInterval(bounds=(1,len(timeslot_index)))

model = pe.ConcreteModel()

model.players = pe.Set(initialize = player_names)

model.assignments = pe.Var(player_names, within = time_domain)

model.available = pe.Param(model.players,
                           model.initialize = available)

model.time = pe.Constraint( player_names, rule = time_rule)

